%define SYSTEM_EXIT 60
%define SYSCALL_WRITE 1
%define SYSCALL_READ 0
%define OUT 1
%define IN 0

section .text
 
; test 

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSTEM_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
    .for:
        mov r10b, byte [rdi + rax] ;rdi 
        inc rax
        cmp r10b, 0
        jne .for
    dec rax
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:   
    push rdi
    call string_length
    pop rsi ; rsi = rdi
    mov rdx, rax ; rdx = str.len()
    mov rax, SYSCALL_WRITE
    mov rdi, OUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    ; Принимает код символа и выводит его в stdout
    print_char:
        push rdi
        mov rsi, rsp ; pointer
        mov rdx, 1 ; char.len
        mov rax, 1
        mov rdi, 1 
        syscall
        pop rdi
        ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    ; Инициализация счетчика цифр
    mov rcx, 10
    mov r8, rsp
    xor rdx, rdx
    push 0

.divide_loop:
    div rcx ; Делим RAX на 10
    add dl, '0'
    ; Получаем остаток (цифру) и преобразуем её в ASCII код
    dec rsp
    mov r9b, byte [rsp]
    mov byte [rsp], dl
    xor rdx, rdx ; Сбрасываем остаток от предыдущего деления
    test rax, rax
    jnz .divide_loop

    mov rdi, rsp
    push r8
    call print_string
    pop rsp
ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint ; if positive
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r11, r11
    .for:
        mov dl, byte [rdi + r11]
        cmp dl, byte [rsi + r11]
        jne .not
        inc r11
        test dl, dl
        jne .for
    mov rax, 1
    ret
    .not:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ; read
    mov rdi, 0 ; stdin
    mov rdx, 1 ; one simbol
    sub rsp, 8
    mov rsi, rsp
    syscall
    test rax,rax   ; Если rax равно 0, значит, достигнут конец потока
    jz .exit
    mov rax, [rsp]
    add rsp, 8
    ret
    .exit:
        add rsp, 8
        xor rax, rax
        ret 

; Принимает: адрес начала буфера - rdi, размер буфера -rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    xor r12, r12
    mov r13, rdi
    mov r14, rsi
    .tab:
        call read_char
        test rax,rax
        je .accept
        cmp rax, ' '
        je .tab
        cmp rax, '\n'
        je .tab
        cmp rax, `\t`
        je .tab
    .for:
        mov byte[r12 + r13], al
        inc r12
        cmp r12, r14
        je .error
        call read_char
        test rax,rax
        je .accept
        cmp rax, ' '
        je .accept
        cmp rax, '\n'
        je .accept
        cmp rax, `\t`
        je .accept
        jmp .for
    .accept:
        mov byte[r12 + r13], 0 ; end string
        mov rdx, r12
        mov rax, r13
        jmp .exit
    .error:
        xor rax, rax
    .exit:
        pop r14
        pop r13
        pop r12
        ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor rcx, rcx
    xor r11, r11
    mov r8, 10
    .for:
        mov r11b, byte[rdi]
        inc rdi
        cmp r11b, '9'
        ja .exit
        cmp r11b, '0'
        jb .exit
        cmp r11b, 0
        je .exit
        mul r8 ; rax *= 10
        sub r11b, '0' ; num - '0'
        add rax, r11; rax += r11
        inc rcx
        jmp .for

    .exit:
        mov rdx, rcx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку - rdi, 
;указатель на буфер - rsi и 
;длину буфера - rdx
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .for:
        cmp rax, rdx
        je .error
        mov r10b, byte[rdi + rax]
        mov byte[rsi + rax], r10b
        inc rax
        test r10b, r10b
        jne .for
    ret
    .error:
        xor rax, rax
        ret
